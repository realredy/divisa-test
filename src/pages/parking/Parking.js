import React, { useState, useEffect } from "react";
import parkingJson from "../../utils/mookParking.json";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import customMarker from "../../utils/customMarker";
import "leaflet/dist/leaflet.css";
import "./parking.scss";

function Parking() {
  const [parking, setParking] = useState();

  return (
    <main className="parking">
      <section className="parking-wrapper">
        <aside className="parking-wrapper-selector">
          <div className="parking-wrapper-selector-title">
            <div className="parking-wrapper-selector-title-wrapper">
              <h2>lista de parking</h2>
              <span onClick={() => setParking(null)}>
                🔄 Ver lista completa
              </span>
            </div>
            <div className="parking-wrapper-selector-list">
              <ul>
                {parkingJson.graph &&
                  parkingJson.graph.map((parkingData, key) => {
                    const latitude = parkingData.location.latitude;
                    const longitude = parkingData.location.longitude;
                    const direccion = parkingData.address["street-address"];
                    const outGoogle = `https://www.google.es/maps/dir/${latitude},${longitude}`;

                    return (
                      <li
                        key={key}
                        onClick={() =>
                          setParking([
                            latitude,
                            longitude,
                            direccion,
                            outGoogle,
                          ])
                        }
                      >
                        <span>{direccion}</span>
                      </li>
                    );
                  })}
              </ul>
            </div>
          </div>
        </aside>
        <section className="parking-wrapper-map">
          <MapContainer
            style={{ height: "100vh" }}
            center={[40.4128, -3.65423]} //40.4128557,-3.6442269,12.67z
            zoom={12}
            scrollWheelZoom={true}
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
            />
            {!parking &&
              parkingJson.graph &&
              parkingJson.graph.map((parkingData, key) => {
                const latitude = parkingData.location.latitude;
                const longitude = parkingData.location.longitude;
                const direccion = parkingData.address["street-address"];
                const outGoogle = `https://www.google.es/maps/dir/${latitude},${longitude}`;
                return (
                  <Marker
                    key={key}
                    icon={customMarker}
                    position={[latitude, longitude]}
                  >
                    <Popup>
                      {direccion}
                      <br />
                      <a href={outGoogle}>Como llegar!</a>
                    </Popup>
                  </Marker>
                );
              })}
            {parking && (
              <Marker icon={customMarker} position={[parking[0], parking[1]]}>
                <Popup>
                  {parking[2]} <br />
                  <a href={parking[3]}>Como llegar!</a>
                </Popup>
              </Marker>
            )}
          </MapContainer>
        </section>
      </section>
    </main>
  );
}

export default Parking;
