import React from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import divisa from "../../images/DIVISA.png";
import "./header.scss";
export default function Header() {
  return (
    <>
      <div className="divisaHeader">
        <div className="divisaHeader-wrapper">
          <div className="divisaHeader-wrapper-inside">
            <div className="divisaHeader-wrapper-inside-logo">
              <img src={divisa} alt="divisa logo" />
              <span>Test By: Ricardo Perez</span>
            </div>
            <Router>
              <ul className="divisaHeader-wrapper-inside-menu">
                <li>
                  <a href="/">Home</a>
                </li>
                <li>
                  <a href="/Parking">parking</a>
                </li>
              </ul>
            </Router>
          </div>
        </div>
      </div>
    </>
  );
}
