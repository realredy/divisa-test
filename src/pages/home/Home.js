import React, { useState, useEffect } from "react";
import "./Home.scss";

function Home() {
  const URL = process.env.REACT_APP_RAMDOMUSER;

  const [users, setUsers] = useState();

  useEffect(async () => {
    try {
      await fetch(URL)
        .then((response) => response.json())
        .then((data) => {
          setUsers(data.results);
        });
    } catch (error) {
      console.log("🤮 error: ", error);
    }
  }, []);

  if (!users)
    return (
      <div className="Loadgin">
        {" "}
        <p> Loading new users...</p>
      </div>
    );
  return (
    <main className="users">
      <section className="users-wrapper">
        {users.map((user) => {
          return (
            <article className="users-wrapper-items">
              <div className="users-wrapper-items-userImageBox">
                <img
                  className="users-wrapper-items-userImageBox-img"
                  src={user.picture.large}
                />
              </div>
              <div className="users-wrapper-items-nickName">
                <span>
                  <b>{user.login.username}</b>
                </span>
              </div>
              <div className="users-wrapper-items-phone">
                <span>
                  <b>Phone: {user.phone}</b>
                </span>
              </div>
            </article>
          );
        })}
      </section>
    </main>
  );

  //===============================================================
  // {
  //   users &&
  //     users.results.map((user) => {
  //       return <>hols</>;
  //     });
  // }

  // return (
  //   <>
  //     {users &&
  //       users.results.map((user, key) => {
  //         return (
  //           <div className="user" key={key}>
  //             <div className="user-image">
  //               <img src={user.results[key].picture.large} alt="user" />
  //             </div>
  //             <div className="user-info">
  //               <h2>
  //                 {user.results[key].name.first} {user.results[key].name.last}
  //               </h2>
  //               <p>
  //                 <span>
  //                   <strong>
  //                     <i className="fas fa-envelope"></i>
  //                   </strong>
  //                   {user.results[key].email}
  //                 </span>
  //                 <span>
  //                   <strong>
  //                     <i className="fas fa-phone"></i>
  //                   </strong>
  //                   {user.results[key].phone}
  //                 </span>
  //               </p>
  //             </div>
  //           </div>
  //         );
  //       })}
  //   </>
  // );
}

export default Home;
