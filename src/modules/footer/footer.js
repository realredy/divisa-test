import React from "react";
import "./footer.scss";

function Footer() {
  return (
    <div className="footer">
      <div className="footer-wrapper">
        <ul className="footer-wrapper-lists">
          <h2>Implementaciones</h2>
          <li>ReactJS</li>
          <li>react-router-dom</li>
          <li>Variables de entorno</li>
          <li>React-leaftlet</li>
          <li>node-sass</li>
          <li>Hooks</li>
        </ul>

        <ul className="footer-wrapper-lists">
          <h2>Implementaciones 2</h2>
          <li>Link Google maps</li>
          <li>custom leaftlet icon</li>
          <li>async-await</li>
          <li>try-catch</li>
          <li>random user api</li>
          <li>json Mook</li>
        </ul>

        <ul className="footer-wrapper-lists">
          <h2>Metodologías</h2>
          <li>Bem</li>
          <li>scss</li>
        </ul>
      </div>
    </div>
  );
}

export default Footer;
